<?php
namespace ITG\Authors\Block\Adminhtml;

class Authors extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_author';
		$this->_blockGroup = 'ITG_Authors';
		$this->_headerText = __('Authors');
		$this->_addButtonLabel = __('Create New Author');
		parent::_construct();
	}
}
