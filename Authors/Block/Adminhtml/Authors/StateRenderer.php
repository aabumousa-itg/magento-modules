<?php
namespace ITG\Authors\Block\Adminhtml\Authors;
class StateRenderer extends 
    \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer{
        protected $factory;
        public function render(\Magento\Framework\DataObject $row){
   $authorID = $row->getData($this->getColumn()->getIndex());
   $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
   $collection = $objectManager->create('Magento\Directory\Model\Region')->load($authorID);

   //Load Customer collection by Customer ID
   return $collection['default_name'];
 }
}
