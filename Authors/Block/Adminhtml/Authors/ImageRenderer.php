<?php 
namespace ITG\Authors\Block\Adminhtml\Authors;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\Object;
use \Magento\Store\Model\StoreManagerInterface;

class ImageRenderer extends AbstractRenderer
{
    protected $_storeManager;


    public function __construct(
        StoreManagerInterface $storeManager

    ){
        $this->_storeManager = $storeManager;

    }
/**
* Renders grid column
*
* @param Object $row
* @return  string
*/
public function render(\Magento\Framework\DataObject $row)
{
$mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
);
// return 'testing';
return '<img src="'.$this->_getValue($row).'"width="50" /> ';
}
}