<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Backup grid item renderer
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace ITG\Authors\Block\Adminhtml\Authors;

class EditActionRenderer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    /**
     * Renders grid column
     *
     * @param \Magento\Framework\DataObject $row
     * @return mixed
     */
    public function _getValue(\Magento\Framework\DataObject $row)
    {
        // $url7zip = __(
        //     'The archive can be uncompressed with <a href="%1">%2</a> on Windows systems.',
        //     'http://www.7-zip.org/',
        //     '7-Zip'
        // );

        return '<a href="' . $this->getUrl(
            '*/*/edit',
            ['id' => $row->getData('id')]
        ) . '"> Edit</a> &nbsp; <small>( beep)</small>';
    }
}
