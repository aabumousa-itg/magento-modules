<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ITG\Authors\Controller\Adminhtml\Authors;
use ITG\Authors\Model\Author;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            foreach($id as $i){
			$model = $this->_objectManager->create(Author::class);
            $model->load($i);
            try {
                $model->delete();
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('authors/authors/index');

            }
            $this->messageManager->addSuccess(__('You deleted the author attrbute.'));
            return $resultRedirect->setPath('authors/authors/index');

        }
    }
        $this->messageManager->addError(__('We can\'t find an author to delete.'));
        return $resultRedirect->setPath('catalog/*/');
    }
}
