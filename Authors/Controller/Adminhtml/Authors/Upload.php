<?php

namespace ITG\Authors\Controller\Adminhtml\Authors;

use Magento\Framework\Controller\ResultFactory;

class Upload extends \Magento\Backend\App\Action
{
    public $imageUploader;
    protected $resultFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \ITG\Authors\Model\ImageUploader $imageUploader,
        ResultFactory $resultFactory
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->resultFactory=$resultFactory;
    }

    public function execute()
    {
        try {
            $result = $this->imageUploader->saveFileToTmpDir('image');
            $result['cookie'] = [
                'name' => $this->_getSession()->getName(),
                'value' => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path' => $this->_getSession()->getCookiePath(),
                'domain' => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}