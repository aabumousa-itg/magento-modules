<?php

namespace ITG\Authors\Controller\Adminhtml\Authors;
use ITG\Authors\Model\Author;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;

class NewAction extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
    protected $uploaderFactory;
    protected $adapterFactory;
    protected $filesystem;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem

	)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
		$this->uploaderFactory = $uploaderFactory;
		$this->adapterFactory = $adapterFactory;
		$this->filesystem = $filesystem;
	
	}

	public function execute()
	{
		
		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend((__('Authors')));
				$contactDatas = $this->getRequest()->getParam('author');
        if(is_array($contactDatas)) {
			$model = $this->_objectManager->create(Author::class);
			$model->setName($contactDatas['name']);
			$model->setEmail($contactDatas['email']);
			if(isset($contactDatas['region_id'])&&$contactDatas['region_id']!=''){
			$model->setRegionId($contactDatas['region_id']);
			}
            $model->setCountryId($contactDatas['country_id']);
			$model->setImage($contactDatas['image'][0]['url']);
            $model->save();
            return $resultPage;
        }
		return $resultPage;
	}
}


// <?php
// namespace ITG\Authors\Controller\Adminhtml\Authors;
// use Magento\Backend\App\Action;
// use ITG\Authors\Model\Author as Author;

// class NewAuthor extends \Magento\Backend\App\Action
// {
//     protected $resultPageFactory = false;

//     public function execute()
//     {
//         $this->_view->loadLayout();
//         $this->_view->renderLayout();

//         $contactDatas = $this->getRequest()->getParam('author');
//         if(is_array($contactDatas)) {
//             $contact = $this->_objectManager->create(Author::class);
//             $contact->setData($contactDatas)->save();
//             $resultRedirect = $this->resultRedirectFactory->create();
//             return $resultRedirect->setPath('*/*/index');
//         }
//     }
// }
