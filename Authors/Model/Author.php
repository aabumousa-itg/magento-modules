<?php

namespace ITG\Authors\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;

class Author extends AbstractModel implements IdentityInterface
{

    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\Authors\Model\ResourceModel\Author');
    }

    

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return ['custom_authors' . '_' . 'custom_authors'];
    }
    public function getRegionName(){
        
    }

}
