<?php
namespace ITG\GPCustom\Model\ResourceModel\Link;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\GPCustom\Model\Link', 'ITG\GPCustom\Model\ResourceModel\Link');
    }
}