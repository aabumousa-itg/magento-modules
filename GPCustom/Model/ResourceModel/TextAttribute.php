<?php

namespace ITG\GPCustom\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TextAttribute extends AbstractDb
{
    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('catalog_product_link_attribute_varchar', 'value_id');
    }
}
