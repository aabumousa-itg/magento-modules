<?php

namespace ITG\GPCustom\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;

class TextAttribute extends AbstractModel implements IdentityInterface
{

    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\GPCustom\Model\ResourceModel\TextAttribute');
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return ['catalog_product_link_attribute_varchar' . '_' . 'catalog_product_link_attribute_varchar'];
    }

}