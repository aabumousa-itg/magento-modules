<?php

namespace ITG\GPCustom\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;

class Link extends AbstractModel implements IdentityInterface
{

    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\GPCustom\Model\ResourceModel\Link');
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return ['catalog_product_link' . '_' . 'catalog_product_link'];
    }

}
