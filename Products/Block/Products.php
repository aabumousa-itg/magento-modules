<?php

namespace ITG\Products\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \ITG\Products\Model\ResourceModel\Product\Collection as productCollection;
use \ITG\Products\Model\ResourceModel\Product\CollectionFactory as productCollectionFactory;

class Products extends Template
{

    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_productCollectionFactory = null;

    /**
     * 
     */
    protected $_catCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param ProductCollectionFactory $productCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_catCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
    * @return array[]
    */
    public function getProducts()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return $collection;

    }
  
    /**
    * @return array[]
    */
    public function getCategories()
    {
        $collection = $this->_catCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return $collection;

    }


}
