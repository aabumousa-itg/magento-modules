define([
    "jquery", "Magento_Ui/js/modal/modal"
], function($,config, element){
  
    var ExampleModal = {
        initModal: function(config, element) {
            $target = $(config.target);
            $target.modal();
            $target.modal({
                getModalOptions: function() { 
                    /** * Modal options */ 
                    var options = { 
                      type: 'popup', 
                      responsive: true, 
                      clickableOverlay: false, 
                      title: $.mage.__('PopUp'), 
                      modalClass: 'popup', 
                      buttons: [{ 
                         text: $.mage.__('Yes, I got you!'), 
                         class: '', 
                         click: function () { 
                            this.closeModal(); 
                         } 
                      }] 
                    };  
                    return options; 
                },
                closed: function(){
                    alert("closed");
                }
    

            });
            $element = $(element);
            $element.click(function() {
                $target.modal('openModal');
            });
        }
    };
    return {
        'example-modal': ExampleModal.initModal
    };
}
);