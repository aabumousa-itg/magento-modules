require(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function(
        $,
        modal,
        target,
        element
    ) {
        console.log(target);
        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            buttons: [{
                text: $.mage.__('Confirm and Continue'),
                class: 'action tocart primary',
                click: function () {
                    this.closeModal();
                }
            },
            {
                text: $.mage.__('Cancel'),
                class: 'action secondary',
                click: function () {
                    
                    this.closeModal();
                }
            }
        ]
        };
      
    
        var popup = modal(options, $('#popup-mpdal'));
        // $element = $(element);

        // $element.click(function() {
        //     console.log("beeeep");
        //     $("#popup-mpdal").modal("openModal");
        // });

        // $("#click-me").on('click',function(){
        //     $("#popup-mpdal").modal("openModal");
        // });
        $("button[name=addto]").on('click',function(){
            $("#popup-mpdal").modal("openModal");
        });

    }
);