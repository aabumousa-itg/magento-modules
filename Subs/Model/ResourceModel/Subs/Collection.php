<?php
namespace ITG\Subs\Model\ResourceModel\Subs;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\Subs\Model\Subs', 'ITG\Subs\Model\ResourceModel\Subs');
    }
}