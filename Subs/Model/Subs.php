<?php

namespace ITG\Subs\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;

class Subs extends AbstractModel implements IdentityInterface
{

    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\Subs\Model\ResourceModel\Subs');
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return ['itg_subscriptions_entity' . '_' . 'itg_subscriptions_entity'];
    }

}
