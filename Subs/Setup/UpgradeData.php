<?php

namespace ITG\Subs\Setup;

use \Magento\Framework\Setup\UpgradeDataInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class UpgradeData
 *
 * @package ITG\Subs\Setup
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * Creates sample blog posts
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.1.9') < 0
        ) {
            $tableName = $setup->getTable('itg_subscriptions_entity');
            $data = [
                [
                    'name' => 'Automatic Shipments',
                    'terms' => 'by selecting automatic shipment, you will receive and be billed for future updates.',
                ],
                [
                    'name' => 'One-time purchase',
                    'terms' => 'by selecting automatic shipment, you will receive and be billed for one-time purchase updates.',
                ],
            ];

            $setup
                ->getConnection()
                ->insertMultiple($tableName, $data);
        }

        $setup->endSetup();
    }
}