<?php
namespace ITG\Subs\Setup;

use \Magento\Framework\Setup\UpgradeSchemaInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package ITG\Authors\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Install Blog Posts table
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer=$setup->startSetup();

        $tableName = $installer->getTable('custom_authors');

        if ($installer->getConnection()->isTableExists($tableName) != true) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Name'
                )
                ->addColumn(
                    'email',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Email'
                )
                ->addColumn(
                    'image',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Image'
                )
                ->addColumn(
                    'region_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['unsigned' => true,'nullable' => false ,  'default' => '0'],
                    'Region ID'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addForeignKey(
                $installer->getFkName('custom_authors','region_id','directory_country_region','region_id'),'region_id',
        $installer->getTable('directory_country_region'), 
        'region_id',
        Table::ACTION_NO_ACTION
        )
                ->setComment('ITG Authors');
            $installer->getConnection()->createTable($table);
        
        }
        
    $setup->endSetup();


}

}