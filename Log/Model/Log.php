<?php

namespace ITG\Log\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;

class Log extends AbstractModel implements IdentityInterface
{

    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\Log\Model\ResourceModel\Log');
    }

    

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return ['custom_user_details' . '_' . 'custom_user_details'];
    }

}
