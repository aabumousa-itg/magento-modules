<?php

namespace ITG\Log\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use ITG\Log\Model\LogFactory;

class CustomerLog implements ObserverInterface
{
    protected $httpHeader;
    protected $remoteIp;
    protected $factory;
    
    
public function __construct(
    \Magento\Framework\App\Action\Context $context,
    LogFactory  $logFactory,
    \Magento\Framework\HTTP\Header $httpHeader,
    \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteIp
){

    $this->httpHeader = $httpHeader;
    $this->remoteIp = $remoteIp;
    $this->factory=$logFactory;
}

    public function execute(Observer $observer)
    {
        
        $userAgent = $this->httpHeader->getHttpUserAgent();
        $remote_ip = $this->remoteIp->getRemoteAddress();
    
        echo $userAgent."-----";
        echo $remote_ip."-----";
        $customer = $observer->getEvent()->getCustomer()->getId();
        echo $customer; //Get customer name
        try{
            $log = $this->factory->create();
            $log->setCustomerId($customer);
            $log->setRemoteIp($remote_ip);
            $log->setUserAgent($userAgent);
            $log->save();
            

            }catch(\Exception $e){
            echo $e->getMessage();
        }


    }

    }
