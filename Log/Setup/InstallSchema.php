<?php

namespace ITG\Log\Setup;

use \Magento\Framework\Setup\InstallSchemaInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package ITG\Crud\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install Blog Posts table
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer=$setup->startSetup();

        $tableName = $installer->getTable('custom_customer_log');

        if ($installer->getConnection()->isTableExists($tableName) != true) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Customer ID'
                )
                ->addColumn(
                    'user_agent',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'USER AGENT'
                )
                ->addColumn(
                    'remote_ip',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'IP'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addForeignKey(
                $installer->getFkName('custom_customer_log','customer_id','customer_log','customer_id'),'customer_id',
        $installer->getTable('customer_log'), 
        'customer_id',
        Table::ACTION_CASCADE
        )
                ->setComment('ITG Crud - Customers Logs');
            $installer->getConnection()->createTable($table);
        
        }
        
    $setup->endSetup();

}

}