<?php
namespace ITG\Log\Controller\Log;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\View\Result\Page;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Registry;

class History extends Action
{
    
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $customerSession;
    protected $registry;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     *
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $_customerSession,
        Registry $coreRegistry,

        PageFactory $resultPageFactory
        
    ) {
        parent::__construct(
            $context
        );
        $this->customerSession=$_customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->registry=$coreRegistry;
    }

    /**
     * Prints the blog from informed order id
     * @return Page
     * @throws LocalizedException
     */
    public function execute()
    {
        
         $id= $this->customerSession->getId();
                 $this->registry->register('CUSTOMER_LOGIN_ID', $id);

        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
