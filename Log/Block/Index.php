<?php
namespace ITG\Log\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \ITG\Log\Model\Log as Log;
use \ITG\Log\Model\ResourceModel\Log\CollectionFactory as LogFactory;
use \Magento\Framework\Registry;
use \ITG\Log\Controller\Log\History as ViewAction;

class Index extends Template
{

    /**
     * CollectionFactory
     * 
     * @var null|LogFactory
     */
    protected $factory = null;

    /**
     * Registry
     * 
     * @var null|Registry
     */
    protected $registry = null;

    /**
     * Session
     * 
     * @var null|Session
     */
    protected $customerSession=null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param array $data
     * @param \Magento\Customer\Model\Session $_customerSession
     * @param \Magento\Framework\Registry $registry
     * @param \ITG\Log\Model\ResourceModel\Log\CollectionFactory $logFactory
     */
    public function __construct(
        Context $context,
        LogFactory $logFactory,
        Registry $coreRegistry,
        \Magento\Customer\Model\Session $_customerSession,
        array $data = []
    ) {
        $this->factory = $logFactory;
        $this->customerSession = $_customerSession;
        $this->registry=$coreRegistry;
        parent::__construct($context, $data);
    }

    /**
    * @return null|array[]
    */
    public function getLogs()
    {
        $id=$this->getId();
        $logs = $this->factory->create();
        $logs->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', array('eq' => $id.''))
            ->setOrder('created_at','DESC')

            ->load();
        return $logs->getItems();
    }

    /**
    * @return null|int
    */
    public function getId(){
        return (int) $this->registry->registry(
            'CUSTOMER_LOGIN_ID'
        );
        
    }

}
