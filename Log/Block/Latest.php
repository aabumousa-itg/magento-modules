<?php
namespace ITG\Log\Block;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Registry;
use \ITG\Log\Model\ResourceModel\Log\CollectionFactory as LogFactory;
use \Magento\Customer\Helper\Session\CurrentCustomer;

class Latest extends Template
{

     /**
     * CollectionFactory
     * 
     * @var null|CollectionFactory
     */
    protected $factory;

     /**
     * CurrentCustomer
     * 
     * @var null|CurrentCustomer
     */
    protected $currentCustomer=null;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param arr\ITG\Log\Model\ResourceModel\Log\CollectionFactory $factory
     * @param array $data
     */
    public function __construct(
        Context $context,
        LogFactory $factory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        array $data = []
    )
    {
        $this->factory = $factory;
        $this->currentCustomer=$currentCustomer;
        parent::__construct($context, $data);
       }
    /**
     * GET customer id from session 
     *
     * @return int
     */
    public function getCustomerId()
    {
            return $this->currentCustomer->getCustomerId();
    }

    /**
     * Lazy loads the requested post
     * @return Log
     * @throws LocalizedException
     */
    public function getLogs()
    {
        $id=$this->getCustomerId();
        
        $logs = $this->factory->create();
        
        $logs->addFieldToSelect('*')
        ->addFieldToFilter('customer_id', array('eq' => $id.''))
        ->setOrder('created_at','DESC')
            ->setPageSize(1) // only get n 
            ->setCurPage(2)     
            ->getFirstItem();
        
        if(sizeof($logs)==0){
            $logs->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', array('eq' => $id.''))
            ->setOrder('created_at','DESC')
                ->setPageSize(1) // only get n 
                ->setCurPage(1)     
                ->getFirstItem();
    
        }
        return $logs;
    }
}
