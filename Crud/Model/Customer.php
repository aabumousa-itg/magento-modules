<?php

namespace ITG\Crud\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;

class Customer extends AbstractModel implements IdentityInterface
{

    /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\Crud\Model\ResourceModel\Customer');
    }

    

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return ['custom_user_details' . '_' . 'custom_user_details'];
    }

    public function setFirstName($name){
        return $this->setData('first_name', $name);

    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData('id');
    }
    /**
     * Get ID
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->getData('first_name');
    }
 /**
     * Get ID
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->getData('last_name');
    }

     /**
     * Get ID
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->getData('phone');
    }


     /**
     * Get ID
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->getData('email');
    }


     /**
     * Get ID
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->getData('image');
    }
  
}
