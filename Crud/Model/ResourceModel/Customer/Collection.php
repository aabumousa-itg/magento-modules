<?php
namespace ITG\Crud\Model\ResourceModel\Customer;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ITG\Crud\Model\Customer', 'ITG\Crud\Model\ResourceModel\Customer');
    }
}
