<?php
namespace ITG\Crud\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;
use ITG\Crud\Model\ResourceModel\Customer\CollectionFactory;
use ITG\Crud\Model\ResourceModel\Customer\Collection;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;

class Create extends \Magento\Framework\App\Action\Action
{
    protected $_customer;
    protected $resultRedirect;
    protected $uploaderFactory;
    protected $adapterFactory;
    protected $filesystem;

    /**
     * @param Context $context
     * @param CustomerFactory $customerFactory
     * @param ResultFactory $result
     *
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */


    public function __construct(
        Context $context,
        CollectionFactory  $customerFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
        ResultFactory $result){
    $this->_customer = $customerFactory;
    $this->resultRedirect = $result;
    $this->uploaderFactory = $uploaderFactory;
    $this->adapterFactory = $adapterFactory;
    $this->filesystem = $filesystem;
    parent::__construct($context);

}


    public function execute()
    {
        
        // 1. POST request : Get booking data
        $customer = (array) $this->getRequest()->getPost();

        if (!empty($customer)) {
            // Retrieve your form data
            
            $model = $this->_objectManager->create('ITG\Crud\Model\Customer');
            $model->setFirstName($customer['first_name']);
            $model->setLastName($customer['last_name']);
            $model->setEmail($customer['email']);
            $model->setPhone($customer['phone']);
            if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                try{
                    $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                    $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    $imageAdapter = $this->adapterFactory->create();
                    $uploaderFactory->addValidateCallback('custom_image_upload',$imageAdapter,'validateUploadFile');
                    $uploaderFactory->setAllowRenameFiles(true);
                    $uploaderFactory->setFilesDispersion(true);
                    $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                    $destinationPath = $mediaDirectory->getAbsolutePath('customer');
                    $result = $uploaderFactory->save($destinationPath);
                    if (!$result) {
                        throw new LocalizedException(
                            __('File cannot be saved to path: $1', $destinationPath)
                        );
                    }
                    $imagePath = 'customer'.$result['file'];
                    $model->setImage($imagePath);
                } catch (\Exception $e) {
        
                }
            $model->save();
            
            // Display the succes form validation message
            // Redirect to your form page (or anywhere you want...)
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/crud/Customer/Create');

            return $resultRedirect;
        }}
        // 2. GET request : Render the booking page 
        $this->_view->loadLayout();

        $this->_view->renderLayout();
    }
}