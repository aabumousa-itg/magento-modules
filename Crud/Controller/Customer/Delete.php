<?php
namespace ITG\Crud\Controller\Customer;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\View\Result\Page;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Exception\LocalizedException;
use \ITG\Crud\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;

class Delete extends Action
{
    protected $resultRedirect;

    /**
     * @var CustomerCollectionFactory
     */
    protected $_factory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param CustomerCollectionFactory $factory
     * @param ResultFactory $factory
     */
    public function __construct(
        Context $context,
        CustomerCollectionFactory $factory,
        ResultFactory $result
    ) {
        $this->_factory = $factory;
        $this->resultRedirect = $result;
        parent::__construct($context);
    }
    public function execute()
    {

        $customer = (array) $this->getRequest()->getPost();
        try {
            $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $myTable = $resource->getTableName('custom_user_details');
            
            $connection->delete(
                $myTable,
                ['id = ?' => $customer['id']]
            );
            $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/crud/Customer/index');

        } catch (\Exception $e) {
            print ($e);
            exit;
            $this->messageManager->addError($e->getMessage());
        }

    }
}
