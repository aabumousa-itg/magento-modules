<?php
namespace ITG\Crud\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;
use ITG\Crud\Model\ResourceModel\Customer\CollectionFactory;
use ITG\Crud\Model\ResourceModel\Customer\Collection;
use ITG\Crud\Model\Customer;
use ITG\Crud\Model\CustomerFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\Registry;

use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;

class Update extends \Magento\Framework\App\Action\Action
{


    const REGISTRY_KEY_POST_ID = 'custom_user_details_id';

    protected $_customer;


    protected $resultRedirect;


    protected $uploaderFactory;


    protected $adapterFactory;


    protected $filesystem;


    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param CustomerFactory $customerFactory
     * @param ResultFactory $result
     *
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */


    public function __construct(
        Context $context,
        CustomerFactory  $customerFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Registry $coreRegistry,
        Filesystem $filesystem,
        ResultFactory $result) {
    $this->_customer = $customerFactory;
    $this->resultRedirect = $result;
    $this->uploaderFactory = $uploaderFactory;
    $this->adapterFactory = $adapterFactory;
    $this->filesystem = $filesystem;
    $this->_coreRegistry=$coreRegistry;
    parent::__construct($context);

}


    public function execute()
    {

        // 1. POST request : Get booking data
        $req = (array) $this->getRequest()->getParams();
        $post = (array) $this->getRequest()->getPost();
        if (!empty($req) && empty($post)) {
            $this->_coreRegistry->register(self::REGISTRY_KEY_POST_ID, (int) $this->_request->getParam('id'));
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        }

        if (!empty($req) && !empty($post)) {
            
            // Retrieve your form data
                            try{

                        $customer=$this->_customer->create()->getCollection()->addFieldToFilter('id',$post['id'])->getFirstItem();                    
                        $customer->setFirstName($post['first_name']); 
                        $customer->setLastName($post['last_name']);
                        $customer->setEmail($post['email']);
                        $customer->setPhone($post['phone']);
                        upload($customer);
                        $customer->save();
                                //  }
                                }
                                   
                            catch(\Exception $e){
                                print ($e->getMessage());
                                exit;
                            }
        }
    }
    private function upload($customer){
        if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                            

        $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
        $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
        $imageAdapter = $this->adapterFactory->create();
        $uploaderFactory->addValidateCallback('custom_image_upload',$imageAdapter,'validateUploadFile');
        $uploaderFactory->setAllowRenameFiles(true);
        $uploaderFactory->setFilesDispersion(true);
        $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $destinationPath = $mediaDirectory->getAbsolutePath('customer');
        $result = $uploaderFactory->save($destinationPath);
        if (!$result) {
            throw new LocalizedException(
                __('File cannot be saved to path: $1', $destinationPath)
            );
        }
        $imagePath = 'customer'.$result['file'];
        $customer->setImage($imagePath);
    }

    }

}