<?php

namespace ITG\Crud\Block;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Registry;
use ITG\Crud\Model\CustomerFactory;
use \ITG\Crud\Controller\Customer\Update as ViewAction;

class Customer extends Template
{
    protected $factory;
    protected $registry;
    protected $customer=null;
    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        CustomerFactory $factory,

        array $data = []
    )
    {
        $this->factory = $factory;
        $this->registry = $coreRegistry;

        parent::__construct($context, $data);
       }

    /**
     * Get form action URL for POST booking request
     *
     * @return string
     */
    public function getFormAction()
    {
            // companymodule is given in routes.xml
            // controller_name is folder name inside controller folder
            // action is php file name inside above controller_name folder

        return '/crud/Customer/Create';
        // here controller_name is index, action is booking
    }
    
    /**
     * Get form action URL 
     *
     * @return array
     */
    public function getCustomerId()
    {
            return (int) $this->registry->registry(
                ViewAction::REGISTRY_KEY_POST_ID
            );
    }

    /**
     * Lazy loads the requested post
     * @return Customer
     * @throws LocalizedException
     */
    public function getCustomer()
    {
        if ($this->customer === null) {
            /** @var Customer $post */
            $customer = $this->factory->create();
            $customer->load($this->getCustomerId());

            if (!$customer->getId()) {
                throw new LocalizedException(__('Post not found'));
            }

            $this->customer = $customer;
        }
        return $this->customer;
    }
}
