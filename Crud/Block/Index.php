<?php

namespace ITG\Crud\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \ITG\Crud\Model\ResourceModel\Customer\Collection as CustomerCollection;
use \ITG\Crud\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;

class Index extends Template
{

    /**
     * CollectionFactory
     * @var null|CustomerCollectionFactory
     */
    protected $_customerCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerCollectionFactory $customerCollectionFactory,
        array $data = []
    ) {
        $this->_customerCollectionFactory = $customerCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
    * @return array[]
    */
    public function getCrud()
    {
        /** @var CustomerCollection $postCollection */
        $customerCollection = $this->_customerCollectionFactory->create();
        $customerCollection->addFieldToSelect('*')->load();
        return $customerCollection->getItems();

    }
  

}
