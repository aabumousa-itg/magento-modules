<?php
namespace ITG\Map\Controller\Map;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\View\Result\Page;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Exception\LocalizedException;

class Index extends Action
{
	
	/**
	* @var PageFactory
	*/
	protected $resultPageFactory;
	protected $_coreRegistry;
	
	/**
	* @param Context $context
	* @param PageFactory $resultPageFactory
	*
	* @codeCoverageIgnore
	* @SuppressWarnings(PHPMD.ExcessiveParameterList)
	*/
	public function __construct(
		Context $context,
		PageFactory $resultPageFactory,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\Registry $registry
		) {
			parent::__construct($context);
			$this->_coreRegistry = $registry;
			$this->scopeConfig = $scopeConfig;
			$this->resultPageFactory = $resultPageFactory;
		}
		
		/**
		* Prints the blog from informed order id
		* @return Page
		* @throws LocalizedException
		*/
		public function execute()
		{            
			$apiKey= $this->scopeConfig->getValue('locator/general/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$id = $this->getRequest()->getParam('place');
			if(isset($id)){
				$address = urlencode($id);
				$this->getCoordinates($address,$apiKey);
			}
			$resultPage = $this->resultPageFactory->create();
			return $resultPage;
		}
		function getCoordinates($address,$key){
			
			$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
			
			$url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$address&key=$key";
			
			$response = file_get_contents($url);
			
			$json = json_decode($response,TRUE); //generate array object from the response from the web
			$i=0;
			$data=[];
			try{
				$items=$json['results'][0]['address_components'];
				$markerCoordenates=$json['results'][0]['geometry']['location'];
				for($i=0; $i<sizeof($items);$i++){
					if($items[$i]['types'][0]=="country"){
						if($json['results'][0]['address_components'][$i]['short_name']=="US"){
							$data[0]="true";
							$data[1]=$markerCoordenates;
							$data[2]=$json['results'][0]['address_components'][$i]['short_name'];
							$this->_coreRegistry->register('country_data', $data);
							return;
						}
					}
				}
				$data[0]="false";
				$data[1]="Place out of bound";
				$this->_coreRegistry->register('country_data', $data);
				return;
				
			}catch(\Exception $e){
				$data[0]="false";
				$this->_coreRegistry->register('country_data', $data);
			}
		}
		
	}