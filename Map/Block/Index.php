<?php
namespace ITG\Map\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Registry;

class Index extends Template
{
    /**
    * ScopeConfigInterface
    * 
    * @var null|ScopeConfigInterface
    */
    protected $scopeConfig = null;
    
    /**
    * Registry
    * 
    * @var null|Registry
    */
    protected $registry = null;
    
    /**
    * Constructor
    *
    * @param Context $context
    * @param array $data
    * @param \Magento\Framework\Registry $coreRegistry
    * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
        ) {
            $this->registry=$coreRegistry;
            $this->scopeConfig = $scopeConfig;
            parent::__construct($context, $data);
        }

        /**
        * Get Map data 
        *
        * @return array
        */
        public function getMapData()
        {
            return (array) $this->registry->registry('country_data');
        }
        
        /**
        * Get API KEY 
        *
        * @return string
        */
        public function getApiKey()
        {
            return $this->scopeConfig->getValue('locator/general/api_key_js', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            
        }
    }
    