<?php
namespace ITG\Info\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	private $eavSetupFactory;

	public function __construct(EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}
	
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'description_custom',
			[
				'type' => 'text',
				'backend' => '',
				'frontend' => '',
				'label' => 'Description Custom',
				'input' => 'text',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => true,
				'user_defined' => false,
				'default' => '',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => true,
				'used_in_product_listing' => true,
				'unique' => false,
				'apply_to' => ''
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'product_classification',
			[
				'type' => 'text',
				'backend' => '',
				'frontend' => '',
				'label' => 'Product Classification',
				'input' => 'select',
				'class' => '',
				'source' => 'ITG\Info\Model\Config\Options',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => true,
				'user_defined' => true,
				'default' => '',
				'searchable' => true,
				'filterable' => true,
				'comparable' => false,
				'visible_on_front' => true,
				'used_in_product_listing' => true,
				'unique' => false,
                'apply_to' => 'simple',
                ]
        );
        $eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'product_brand',
			[
				'type' => 'text',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
				'frontend' => '',
				'label' => 'Product Brand',
				'input' => 'multiselect',
				'class' => '',
				'source' => 'ITG\Info\Model\Config\MultiOptions',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => true,
				'user_defined' => true,
				'default' => '',
				'searchable' => true,
				'filterable' => true,
				'comparable' => false,
				'visible_on_front' => true,
				'used_in_product_listing' => true,
				'unique' => false,
                'apply_to' => 'simple',
                ]
		);
	}
}