<?php
 
namespace ITG\Info\Model\Config;
 
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
    * Get all options
    *
    * @return array
    */
    public function getAllOptions()
    {
        $this->_options = [
                ['label' => __('Chips Bag'), 'value'=>'Chips Bag'],
                ['label' => __('Chips Box'), 'value'=>'Chips Box'],
                ['label' => __('Chips Container'), 'value'=>'Chips Box']
            ];
 
    return $this->_options;
 
    }
 
}