<?php
 
namespace ITG\Info\Model\Config;
 
class MultiOptions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
    * Get all options
    *
    * @return array
    */
    public function getAllOptions()
    {
        $this->_options = [
                ['label' => __('Lays'), 'value'=>'Lays'],
                ['label' => __('Pringles'), 'value'=>'Pringles'],
                ['label' => __('Doritos'), 'value'=>'Doritos']
            ];
 
    return $this->_options;
 
    }
 
}